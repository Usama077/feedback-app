<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

         $user = \App\Models\User::create([
             'name' => 'Admin',
             'email' => 'admin@feedback.com',
             'is_admin' => true,
             'password' => bcrypt('envPassword'),
         ]);
        $admin = Role::create([
            'name' => 'admin'
        ]);

        $userList = Permission::create(['name' => 'UserList']);
        $admin->givePermissionTo($userList);
        $user->assignRole('admin');

    }
}
