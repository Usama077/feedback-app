<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\FeedbackController;
use \App\Http\Controllers\CommentController;
use \App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Route::middleware(['auth'])->group(function () {
    Route::get('/feedback/create', [FeedbackController::class, 'create'])->name('feedback.create');
    Route::post('/feedback/store', [FeedbackController::class, 'store'])->name('feedback.store');
    Route::get('/feedback/listings', [FeedbackController::class, 'list'])->name('feedback.list');
    Route::get('/feedback/detail/{id}', [FeedbackController::class, 'detail'])->name('feedback.detail');

    Route::post('/comment/store/{id}', [CommentController::class, 'store'])->name('comments.store');

    Route::middleware(['role:admin'])->group(function () {
        Route::get('/admin/all-users',[AdminController::class,'allUsers'])->name('admin.users');
        Route::get('/admin/all-comments',[AdminController::class,'allComments'])->name('admin.comments');
        Route::get('/admin/update-comment-status/{id}',[AdminController::class,'updateCommetnStatus'])->name('admin.comments.update');
        Route::get('/admin/delete-user/{id}',[AdminController::class,'deleteUser'])->name('admin.delete.user');
    });

});
