<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
        ]);

        $user = auth()->user();
        $feedback = Feedback::find($id);
        $comment = $feedback->comments()->create([
            'user_id' => $user->id,
            'content' => $request->input('content'),
        ]);

        return redirect()->back();
    }
}
