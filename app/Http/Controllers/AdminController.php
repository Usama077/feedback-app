<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function allUsers()
    {
        $users = User::where('is_admin',false)->get();
        return view('admin.users-list',compact('users'));
    }
    public function allComments()
    {
        $comments = Comment::with('user')->get();
        return view('admin.comments',compact('comments'));
    }
    public function updateCommetnStatus($id)
    {
        $comment = Comment::findOrFail($id);
        if ($comment->is_enable){
            $comment->update([
               'is_enable' => false,
            ]);
        }else{
            $comment->update([
                'is_enable' => true,
            ]);
        }
        return redirect()->back();
    }

    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->back();
    }
}
