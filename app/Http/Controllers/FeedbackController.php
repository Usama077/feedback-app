<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function create()
    {
        return view('feedback.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
            'category' => 'required',
        ]);

        $feedback = auth()->user()->feedbacks()->create($request->all());

        return redirect()->route('feedback.create')->with('success', 'Feedback submitted successfully!');
    }

    public function list()
    {
        $feedbacks = Feedback::get();
        return view('feedback.list',compact('feedbacks'));
    }

    public function detail($id)
    {
        $feedback = Feedback::findOrFail($id);
        return view('feedback.detail',compact('feedback'));
    }
}
