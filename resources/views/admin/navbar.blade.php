<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light shadow fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#"><img src="{{ asset('logo-color.png') }}" style="width: 40%;" alt="Logo" >
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarResponsive">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item {{url()->current() == route('admin.users') ? 'active' : '' }}" style="padding-top: 7px;">
                    <a class="nav-link" href="{{route('admin.users')}}">Users</a>
                </li>
                <li class="nav-item {{url()->current() == route('admin.comments') ? 'active' : '' }}" style="padding-top: 7px;">
                    <a class="nav-link" href="{{route('admin.comments')}}">Comments</a>
                </li>
                <li class="nav-item">
                    <form class="nav-link" action="{{route('logout')}}" method="post">
                        @csrf
                        <button class="btn btn-primary" type="submit">Logout</button>
                    </form>
                </li>
            </ul>

        </div>
    </div>
</nav>
