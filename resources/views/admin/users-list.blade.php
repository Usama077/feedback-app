@extends('admin.main')

@section('title', 'Submit Feedback')

<style>
    .listing{
        /*width: -webkit-fill-available;      */
        width: 85%;
    }
    .card-header{
        background: #1399ce !important;
        color: white;
    }
    .btn-primary{
        background: #1399ce !important;
        box-shadow: none !important;;

    }
    .btn-primary:focus{
        box-shadow: none !important;
        border: none !important;
    }
    .btn-primary:active{
        box-shadow: none !important;
        border: none !important;
    }

</style>
@section('content')
    <div class=" d-flex justify-content-center align-items-center min-vh-100 ">
        <div class="card shadow p-3 mb-5 bg-white rounded listing">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">All Users</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-responsive-md" id="feedbackTable">
                                <thead>
                                <tr>
                                    <th style="width:80px;">#</th>
                                    <th>Name</th>
                                    <th>Mail</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $key => $user)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td><small>
                                                <a href="{{route('admin.delete.user',$user->id)}}"
                                                   class="btn btn-danger text-white">
                                                    Delete
                                                </a>
                                            </small>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
