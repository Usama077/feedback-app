@extends('admin.main')

@section('title', 'Submit Feedback')

<style>
    .listing{
        /*width: -webkit-fill-available;      */
        width: 85%;
    }
    .card-header{
        background: #1399ce !important;
        color: white;
    }
    .btn-primary{
        background: #1399ce !important;
        box-shadow: none !important;;

    }
    .btn-primary:focus{
        box-shadow: none !important;
        border: none !important;
    }
    .btn-primary:active{
        box-shadow: none !important;
        border: none !important;
    }

</style>
@section('content')
    <div class=" d-flex justify-content-center align-items-center min-vh-100 ">
        <div class="card shadow p-3 mb-5 bg-white rounded listing" style="margin-top: 100px;">
            <div class="col-lg-12" >
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">All Comments</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-responsive-md" id="feedbackTable">
                                <thead>
                                <tr>
                                    <th style="width:80px;">#</th>
                                    <th>Name</th>
                                    <th>Comment</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($comments as $key => $comment)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{ $comment->user->name }}</td>
                                        <td>{{ $comment->content }}</td>
                                        <td class="text-center align-middle status-td">
                                            @if($comment->is_enable)
                                                <div class="d-flex justify-content-center">
                                                    <a href="{{route('admin.comments.update',$comment->id)}}"
                                                       class="d-block btn pad-1px btn-danger"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Disable from here" type="button">Disable</a>
                                                </div>
                                            @else
                                                <div class="d-flex justify-content-center">
                                                    <a href="{{route('admin.comments.update',$comment->id)}}"
                                                        id="deactivated_btn"
                                                        class="d-block btn pad-1px btn-success"
                                                        data-toggle="tooltip" data-placement="top"
                                                        title="Enable from here" type="button">Enable
                                                    </a>
                                                </div>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
