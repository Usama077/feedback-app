@if(session('success'))
    <div class="d-flex justify-content-center" style="margin-top: 50px;">
        <div style="width: 75%;" id="success-alert" class="alert alert-success alert-dismissible fade show text-center"
             role="alert" data-bs-delay="5000">
            <strong>Success:</strong> <br> {{ session('success') }}
            <span id="success-timer" class="float-start"></span>
{{--            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>--}}
        </div>
    </div>
@endif
@if(session('error'))
    <div class="d-flex justify-content-center">
        <div style="width: 75%;" id="error-alert" class="alert alert-danger alert-dismissible fade show text-center"
             role="alert" data-bs-delay="5000">
            <strong>Error:</strong> <br> {{ session('error') }}
            <span id="error-timer" class="float-start"></span>
{{--            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>--}}
        </div>
    </div>
@endif
{{--@if(session('success') || session('error'))--}}
{{--    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>--}}
{{--    <script>--}}
{{--        // Automatically close success and error alerts after 5 seconds--}}
{{--        window.setTimeout(function () {--}}
{{--            $("#success-alert, #error-alert").alert('close');--}}
{{--        }, 5000);--}}

{{--        // Countdown timer--}}
{{--        function startCountdownTimer(elementId, duration) {--}}
{{--            let timerElement = document.getElementById(elementId);--}}

{{--            let intervalId = setInterval(function () {--}}
{{--                duration -= 1000;--}}
{{--                let seconds = Math.floor((duration % (1000 * 60)) / 1000);--}}

{{--                if (duration <= 0) {--}}
{{--                    clearInterval(intervalId);--}}
{{--                }--}}

{{--                timerElement.innerText = seconds + "s";--}}
{{--            }, 1000);--}}
{{--        }--}}

{{--        // Start countdown timers--}}
{{--        startCountdownTimer('success-timer', 5000);--}}
{{--        startCountdownTimer('error-timer', 5000);--}}
{{--    </script>--}}
{{--@endif--}}
