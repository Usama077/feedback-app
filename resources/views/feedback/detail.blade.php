@extends('layouts.app')

@section('title', 'Submit Feedback')
@push('styles')
    <style>
        body {

            background-color: #eee;
        }

        .card {

            background-color: #fff;
            border: none;
        }

        .form-color {

            background-color: #fafafa;

        }

        .form-control {

            height: 48px;
            border-radius: 25px;
        }

        .form-control:focus {
            color: #495057;
            background-color: #fff;
            border-color: #35b69f;
            outline: 0;
            box-shadow: none;
            text-indent: 10px;
        }

        .c-badge {
            background-color: #35b69f;
            color: white;
            height: 20px;
            font-size: 11px;
            width: 92px;
            border-radius: 5px;
            display: flex;
            justify-content: center;
            align-items: center;
            margin-top: 2px;
        }

        .comment-text {
            font-size: 13px;
        }

        .wish {

            color: #35b69f;
        }


        .user-feed {

            font-size: 14px;
            margin-top: 12px;
        }

    /*    new*/
        body {
            background-color: #eee;

        }

        .bdge {
            height: 21px;
            background-color: #1399ce;
            color: #fff;
            font-size: 11px;
            padding: 8px;
            border-radius: 4px;
            line-height: 3px;
        }

        .comments {
            text-decoration: underline;
            text-underline-position: under;
            cursor: pointer;
        }

        .dot {
            height: 7px;
            width: 7px;
            margin-top: 3px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;
        }

        .hit-voting:hover {
            color: blue;
        }

        .hit-voting {
            cursor: pointer;
        }
        .btn-primary{
            background: #1399ce !important;
            box-shadow: none !important;
            border: none !important;

        }
        .btn-primary:focus{
            box-shadow: none !important;
            border: none !important;
        }
        .btn-primary:active{
            box-shadow: none !important;
            border: none !important;
        }

    </style>
@endpush

@section('content')
{{--    <div class="d-flex justify-content-center align-items-center min-vh-100">--}}
{{--        <div class="container mt-4 ">--}}
{{--            <div class="card mb-4">--}}
{{--                <div class="card-body">--}}
{{--                    <h2 class="card-title">{{ $feedback->title }}</h2>--}}
{{--                    <p class="card-text">{{ $feedback->description }}</p>--}}
{{--                    <p class="card-text"><strong>Category:</strong> {{ $feedback->category }}</p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="container mt-2 mb-5">--}}
{{--                <div class="row height d-flex justify-content-center align-items-center">--}}
{{--                    <div class="col-md-7">--}}
{{--                        <div class="card">--}}
{{--                            <div class="p-3">--}}
{{--                                <h6>Comments</h6>--}}
{{--                            </div>--}}
{{--                            <form action="{{ route('comments.store', $feedback->id )}}" method="post">--}}
{{--                                @csrf--}}

{{--                                <div class="mt-3 d-flex flex-row align-items-center p-3 form-color">--}}
{{--                                    <input type="text" name="content" class="form-control" placeholder="Enter your comment...">--}}
{{--                                </div>--}}
{{--                                <button style="margin-left: 14px;" class="form-control col-4 btn btn-primary"--}}
{{--                                        type="submit">Submit--}}
{{--                                </button>--}}
{{--                            </form>--}}
{{--                            <div class="mt-2">--}}
{{--                                @foreach($feedback->comments as $comment)--}}
{{--                                <div class="d-flex flex-row p-3">--}}
{{--                                    <div class="w-100">--}}
{{--                                        <div class="d-flex justify-content-between align-items-center">--}}
{{--                                            <div class="d-flex flex-row align-items-center">--}}
{{--                                                <span class="mr-2">{{ $comment->user->name }}</span>--}}
{{--                                            </div>--}}
{{--                                            <small>{{ $comment->created_at->diffForHumans() }}</small>--}}
{{--                                        </div>--}}
{{--                                        <p class="text-justify comment-text mb-0">{{ $comment->content }}</p>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                @endforeach--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="container py-5" >
        <div class="d-flex justify-content-center row">
            <div class="d-flex flex-column col-md-12">
                <div class="d-flex flex-row align-items-center text-left comment-top p-2 bg-white border-bottom">
                     <div class="d-flex flex-column ml-3">
                        <div class="d-flex flex-row post-title py-3">
                            <h2>{{ $feedback->title }}</h2>
                         </div>
                        <div class="d-flex flex-row align-items-center align-content-center post-title">
                            {{$feedback->description }}
                        </div>
                         <div class="d-flex flex-row align-items-center align-content-center post-title">
                              <span class="bdge mr-1">
                                {{ $feedback->category }}
                            </span>
                         </div>
                    </div>
                </div>
                <div class="coment-bottom bg-white p-2 px-4">
                    <form action="{{ route('comments.store', $feedback->id )}}" method="post">
                        @csrf
                    <div class="d-flex flex-row add-comment-section mt-4 mb-4">
                        <input type="text" name="content" id="mention-input" class="form-control mr-3" required placeholder="Add comment">
                        <button class="btn btn-primary" type="submit">Comment</button>
                    </div>
                    </form>
                    @foreach($feedback->comments as $comment)
                        @if($comment->is_enable)
                    <div
                        class="commented-section mt-2">
                        <div class="d-flex flex-row align-items-center commented-user">
                            <h5 class="mr-2">{{ $comment->user->name }}</h5><span class="dot mb-1"></span><span class="mb-1 ml-2">{{ $comment->created_at->diffForHumans() }}</span></div>
                        <div class="comment-text-sm"><span>{{ $comment->content }}</span></div>
                    </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
