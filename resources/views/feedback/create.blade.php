@extends('layouts.app')

@section('title', 'Submit Feedback')
<style>
    .btn-primary{
        background: #1399ce !important;
        box-shadow: none !important;
        border: none !important;

    }
    .btn-primary:focus{
        box-shadow: none !important;
        border: none !important;
    }
    .btn-primary:active{
        box-shadow: none !important;
        border: none !important;
    }
    .card{
        /*width: 35%;*/
        width: -webkit-fill-available;
    }

</style>
@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center" >
            <div class="col-md-12" style="margin-top: 40px;">
                @if(session('success'))
                    <div class="d-flex justify-content-center" style="margin-top: 50px;">
                        <div style="width: 75%;" id="success-alert" class="alert alert-success alert-dismissible fade show text-center"
                             role="alert" data-bs-delay="5000">
                            <strong>Success:</strong> <br> {{ session('success') }}
                            <span id="success-timer" class="float-start"></span>
                        </div>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">FeedBack Submission Form</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('feedback.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="name">Title</label>
                                <input type="text" name="title" class="form-control" id="title" placeholder="Enter title">
                            </div>
                            <div class="form-group">
                                <label for="message">Description</label>
                                <textarea class="form-control" name="description" id="description" rows="3" placeholder="Enter description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="category" class="form-label">Category:</label>
                                <select name="category" id="category" required class="form-control">
                                    <option value="bug report">Bug Report</option>
                                    <option value="feature request">Feature Request</option>
                                    <option value="improvement">Improvement</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary mt-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
